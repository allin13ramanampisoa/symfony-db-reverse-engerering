# Description
App DB Reverse engenering with symfony version 6.2

# Requirement
- PHP <= 8.1

## Configure your .env file
```bash
nano .env
```

## Installation
```bash
composer install
```

## Generate Entity
```bash
php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity
```

## Convert @annotaion to #[attribute] 
```bash
php vendor/bin/rector process
```
